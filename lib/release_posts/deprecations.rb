require 'yaml'
require 'gitlab'
require 'concurrent/array'
require 'concurrent/executors'
require_relative 'helpers'
require_relative '../api_retry'

module ReleasePosts
  class Deprecations
    include Helpers
    include ApiRetry

    GITLAB_API_URL = 'https://gitlab.com/api/v4'.freeze

    def initialize(dryrun: ENV.fetch("DRYRUN", false))
      deprecations_dir = File.join(git_repo_path, 'data', 'release_posts')
      @deprecations_index_file = File.join(deprecations_dir, 'deprecations-index.yml')
      @is_dryrun = dryrun

      configure_gitlab_client
    end

    def generate
      prepare
      save_deprecations
      unless dry_run?
        commit_deprecations
        push_deprecation_changes
      end
      cleanup
    end

    def retrieve_deprecations(reverse_sort_by: :announcement_milestone)
      start_time = Time.now
      log_info("[#{start_time}] Retrieving deprecation files from API...")
      deprecation_files = api_retry do
        Gitlab
          .tree(GITLAB_PROJECT_ID, { path: 'data/deprecations', per_page: 100 })
          .auto_paginate
      end

      pool = Concurrent::FixedThreadPool.new(10)
      deprecations = Concurrent::Array.new

      begin
        deprecation_files.each do |deprecation_file|
          next unless deprecation_file.path.end_with?("yml")

          pool.post do
            built_deprecation = build_deprecation(deprecation_file.path)

            deprecations << built_deprecation if built_deprecation.breaking_change
          rescue StandardError => e
            log_error("Rescued exception when calling `#build_deprecation` in a thread!", e)
          end
        end
      rescue StandardError => e
        log_error("Rescued exception in #{self.class.name}##{__method__}!", e)
        pool.kill # terminate it early because we don't want to wait in case of exceptions
      else
        pool.shutdown # gracefully shutting down
      ensure
        pool.wait_for_termination
      end

      log_info("[#{Time.now}] Loaded #{deprecations.size} deprecation files, sorted by `:#{reverse_sort_by}` in #{Time.now - start_time} seconds")

      deprecations.sort_by(&reverse_sort_by).reverse
    end

    private

    def dry_run?
      @is_dryrun
    end

    def prepare
      dry_run_message = <<~MESSAGE
        *** DRY RUN ***
        This script will not push changes to the deprecations index to GitLab.
      MESSAGE

      log_info(dry_run_message) if dry_run?

      log_info("Fetching branches")
      git_fetch

      log_info('Set username and email')
      git_config('user.email', 'job+bot@gitlab.com')
      git_config('user.name', 'Bot')

      return if dry_run?

      log_info("Switching branch to #{next_release}")
      git_change_branch(next_release)
    end

    def cleanup
      log_info('Unset username and email')
      git_unset_config('user.email')
      git_unset_config('user.name')
    end

    def configure_gitlab_client
      log_info("Connecting to GitLab...")

      Gitlab.configure do |config|
        config.endpoint = GITLAB_API_URL
        config.private_token = ENV['GITLAB_BOT_TOKEN'] || ENV['PRIVATE_TOKEN'] || ''
      end
    end

    def formatted_deprecations
      retrieve_deprecations.group_by(&:announcement_milestone).transform_values { |v| v.map(&:title) }.to_yaml
    end

    def save_deprecations
      File.write(@deprecations_index_file, formatted_deprecations)
    end

    def commit_deprecations
      git_add(@deprecations_index_file)
      git_commit("Updated deprecations content")
    end

    def push_deprecation_changes
      git_push("https://jobbot:#{ENV.fetch('GITLAB_BOT_TOKEN', nil)}@gitlab.com/gitlab-com/www-gitlab-com.git", next_release)
    end

    def fetch_release_branches
      (git_branch_list('origin/release-*') || '').split
    end

    def next_release
      release_branch = fetch_release_branches
        .select { |name| name =~ %r{\Aorigin/release-\d+-\d+\Z} }
        .map { |name| name.delete_prefix('origin/') }
        .max_by { |name| sort_by_version(name) }

      unless release_branch
        log_error("Release branch is missing!")
        exit 1
      end

      log_info("Detected release: #{release_branch}")
      release_branch
    end

    def sort_by_version(release)
      version = release.match(/(\d+)-(\d+)/)
      [version[1].to_i, version[2].to_i]
    end

    def build_deprecation(path)
      Deprecation.new(path, GITLAB_PROJECT_ID)
    rescue StandardError => e
      log_error("Error loading deprecation: #{path}", e)
    end
  end
end
