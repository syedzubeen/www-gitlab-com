#############################################################
# Key Performance Indicators
# This section contains only KPIs
# Ordering the same way it's represented in the handbook.
#############################################################

- name: GitLab.com Availability SLO
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Percentage of time during which GitLab.com is fully operational and providing service to users within SLO parameters. Definition is available on the <a href="https://dashboards.gitlab.net/d/general-slas/general-slas?orgId=1">GitLab.com Service Level Availability page</a>.
    Historical Availability is available on the <a href="https://about.gitlab.com/handbook/engineering/monitoring/#historical-service-level-availability">Service Level Availability page</a>.
  target: equal or greater than 99.80%
  org: Infrastructure Department
  is_key: true
  dri: <a href="https://gitlab.com/meks">Mek Stittri</a>
  health:
    level: 3
    reasons:
      - June 2024 100.00%
      - May 2024 99.99%
      - April 2024 99.95%
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/GitlabAvailabilitySLO
        height: 400px
        toolbar: hidden
        hide_tabs: true
  urls:
    - https://about.gitlab.com/handbook/engineering/monitoring/#historical-service-availability
    - https://dashboards.gitlab.net/d/general-slas/general-slas?orgId=1&from=now%2FM&to=now
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/Gitlab_comAvailabilityKPI
    - https://docs.google.com/spreadsheets/d/1ee7O2k0Krg9PYDta_nehRZM2JMssPm81vS8kv05fu5g/edit#gid=0

- name: Corrective Action SLO
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: The Corrective Actions (CAs) SLO focuses on the number of open severity::1/severity::2 Corrective Action Issues past their due date. Corrective Actions and their due dates are defined in our <a href="/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership">Incident Review process</a>.
  target: below 0
  org: Infrastructure Department
  is_key: true
  dri: <a href="https://gitlab.com/meks">Mek Stittri</a>
  health:
    level: 2
    reasons:
      - Corrective Action SLO is at 2
  urls:
    - https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/CorrectiveActionsKPI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/CorrectiveActionsSLO

- name: Master Pipeline Stability
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures our monolith master pipeline success rate. A key indicator to engineering productivity and the stability of our releases.
     We will continue to leverage <a href="https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/195">Merge Trains</a> in this effort.
  target: Above 95%
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/yanguo1">Ethan Guo</a>
  health:
    level: 2
    reasons:
      - April 2024 decreased to 91%
      - Cause for broken `master` for April 2024 are flaky tests (45%), infrastructure/runner issues (42%), job timing out (17%), various infrastructure issues (11%), failed to pull job image (9%), runner disk full (5%), merge train missing (3%), test gap (3%), dependency upgrade (3%), broken ci config (2%), GitLab.com overloaded (2%)
      - We automated the test quarantine process to remove very disruptive flaky tests from the pipelines and report them to their team's weekly triage report
      - More communication has been added to merge requests and Slack channels to seek earlier actions on failed pipelines
  urls:
    - https://gitlab.com/groups/gitlab-org/-/epics/8789
    - https://gitlab.com/gitlab-org/quality/quality-engineering/team-tasks/-/issues/195
    - https://gitlab.com/groups/gitlab-org/quality/engineering-productivity/-/epics/30
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/MasterPipelineStabilityKPI
    - https://10az.online.tableau.com/#/site/gitlab/views/DraftMasterBrokenIncidentsRootCauseAnalysis/MasterBrokenIncidentsRootCause?:iid=1
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/MasterPipelineStability
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: Merge request pipeline duration
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the average successful duration for the monolith merge request pipelines. Key building block to improve
    our cycle time, and efficiency. More <a href="https://gitlab.com/groups/gitlab-org/-/epics/1853">pipeline improvements</a>.
  target: Below 45 minutes
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/yanguo1">Ethan Guo</a>
  health:
    level: 2
    reasons:
      - |
        The previous chart we were showing made some assumptions on the dependency of CI jobs, and those assumptions do not hold anymore,
        causing our chart to sometimes not take child pipelines into account when computing pipeline duration.
        A fix was made on 2024-04-29 to ensure that we're using the pipeline duration from GitLab database directly instead of calculating from queries ([see investigation](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/378#note_1740584179)).
        As a result, the average duration and percentiles duration are higher than previously thought.
  urls:
    - https://gitlab.com/groups/gitlab-org/-/epics/1853
    - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/140625
    - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139473
    - https://gitlab.com/gitlab-org/gitlab/-/issues/430753
    - https://10az.online.tableau.com/#/site/gitlab/workbooks/2312755/views
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/MergeRequestPipelineDurationKPI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/MergeRequestPipelineDuration
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: S1 Open Customer Bug Age (OCBA)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: S1 Open Customer Bug Age (OCBA) measures the total number of days that all S1 customer-impacting bugs are open within a month divided by the number of S1 customer-impacting bugs within that month.
  target: Below 30 days
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/vincywilson">Vincy Wilson</a>
  health:
    level: 3
    reasons:
      - Promoted to KPI in FY24Q2
      - Near target for 3 consecutive months, uptick in current month due to ongoing triaging of issues
      - All S1 bugs are been reviewed for upcoming milestone planning
  urls:
    - https://gitlab.com/groups/gitlab-org/quality/quality-engineering/-/epics/8
    - https://gitlab.com/gitlab-org/quality/quality-engineering/team-tasks/-/issues/2433 
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/S1OpenCustomerBugAgeKPI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/S1OCBA

- name: S2 Open Customer Bug Age (OCBA)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: S2 Open Customer Bug Age (OCBA) measures the total number of days that all S2 customer-impacting bugs are open within a month divided by the number of S2 customer-impacting bugs within that month.
  target: Below 250
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/vincywilson">Vincy Wilson</a>
  health:
    level: 2
    reasons:
      - Promoted to KPI in FY24Q2
      - Above target, significant reduction will require a focus on older customer impacting S2
  urls:
    - https://gitlab.com/groups/gitlab-org/quality/quality-engineering/-/epics/8
    - https://gitlab.com/gitlab-org/quality/quality-engineering/team-tasks/-/issues/2421
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/S2OpenCustomerBugAgeKPI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/S2OCBA

- name: Quality Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab.
    Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/meks">Mek Stittri</a>
  public: false
  health:
    level: -1
    reasons:
      - Confidential metric, see notes in Key Review agenda
      - Will be merged into a combined department retention metric
  urls:
    - "https://10az.online.tableau.com/#/site/gitlab/views/N5AttritionDashboard/AttritionDashboard?:iid=1"

- name: Infrastructure Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Infrastructure Department
  is_key: true
  dri: <a href="https://gitlab.com/meks">Mek Stittri</a>
  public: false
  health:
    level: -1
    reasons:
      - Confidential metric, see notes in Key Review agenda
      - Will be merged into a combined department retention metric
  urls:
    - "https://10az.online.tableau.com/#/site/gitlab/views/N5AttritionDashboard/AttritionDashboard?:iid=1"


#############################################################
# Regular Performance Indicators
# This section contains only PIs
# This is grouped by the following order
#   - Engineering Productivity
#   - Quality Engineering
#   - Engineering Analytics (TBD)
#   - Department Efficiency
#   - People Metrics
# Please grouped PIs together for ease of review
#############################################################

- name: Mean Time To Production (MTTP)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the elapsed time (in hours) from merging a change in <a href="https://gitlab.com/gitlab-org/gitlab">gitlab-org/gitlab projects</a> master branch, to deploying that change to gitlab.com.
    It serves as an indicator of our speed capabilities to deploy application changes into production. This metric is equivalent to the <i>Lead Time for Changes</i> metric in the <a href="https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance">Four Keys Project from the DevOps Research and Assessment</a>.
    Additionally, the data for this metric also shows <i>Deployment Frequency</i>, another of the Four Keys metrics. MTTP breakdown can be visualized on the <a href="/handbook/engineering/infrastructure/team/delivery/metrics.html"> Delivery Metrics page </a>.
  target: less than 12 hours
  org: Infrastructure Department
  is_key: false
  dri: <a href="https://gitlab.com/amyphillips">Amy Phillips</a>
  health:
    level: 3
    reasons:
      - Work towards MTTP <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/280">epic 280</a>.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/MTTP
        height: 400px
        toolbar: hidden
        hide_tabs: true
  urls:
    - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170
    - "/handbook/engineering/infrastructure/team/delivery/metrics"
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/MeanTimetoProductionKPI

############################
# Engineering Productivity
############################

- name: Review App deployment success rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the stability of our test tooling to enable end to end and exploratory testing feedback.
  target: Above 95%
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/yanguo1">Ethan Guo</a>
  health:
    level: 2
    reasons:
      - Moved to regular PI in FY24Q2
      - Stabilized at 95% to 96% in the past 3 months
  urls:
    - https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/50
    - https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/56
    - https://gitlab.com/groups/gitlab-org/-/epics/7826
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/ReviewAppSuccessRatePI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/ReviewAppSuccessRate
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: Time to First Failure p80
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: TtFF (pronounced "teuf") measures the 80th percentile time from pipeline creation until the first actionable failed build is completed for the GitLab monorepo project.
    We want to run the tests that are likely to fail first and shorten the feedback cycle to R&D teams.
  target: Below 20 minutes
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/yanguo1">Ethan Guo</a>
  health:
    level: 3
    reasons:
      - Track this metric in addition to average starting FY23Q4
      - Plan to optimize selective tests in place for backend and frontend tests
  urls:
    - https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/#test-intelligence
    - https://gitlab.com/gitlab-org/gitlab/-/issues/361665
    - https://gitlab.com/groups/gitlab-org/-/epics/3806
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/TimetoFirstFailureP80PI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/TimetoFirstFailureP80
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: Time to First Failure
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: TtFF (pronounced "teuf") measures the average time from pipeline creation until the first actionable failed build is completed for the GitLab monorepo project.
    We want to run the tests that are likely to fail first and shorten the feedback cycle to R&D teams.
  target: Below 15 minutes
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/yanguo1">Ethan Guo</a>
  health:
    level: 3
    reasons:
      - Moved to regular PI in FY24Q2
      - Under target of 15 mins for the past 2 months
  urls:
    - https://about.gitlab.com/handbook/engineering/quality/engineering-productivity/#test-intelligence
    - https://10az.online.tableau.com/#/site/gitlab/views/DRAFTTestIntelligenceAccuracy/TestIntelligenceAccuracy
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/TimetoFirstFailurePI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/TimetoFirstFailure
        height: 400px
        toolbar: hidden
        hide_tabs: true

############################
# Quality Engineering
############################

- name: Average duration of end-to-end test suite
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the speed of our full QA/end-to-end test suite in
    the <code>master</code> branch. A Software Engineering in Test job-family performance-indicator.
  target: at 90 mins
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/vincywilson">Vincy Wilson</a>
  health:
    level: 3
    reasons:
      - Below target of 90 mins
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/AverageDurationofEnd-to-EndTests
        height: 400px
        toolbar: hidden
        hide_tabs: true
  urls:
    - https://gitlab.com/gitlab-org/quality/team-tasks/issues/198
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/AverageDurationofEnd-to-EndTestSuitePI

- name: Average age of quarantined end-to-end tests
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the stability and effectiveness of our QA/end-to-end tests
    running in the <code>master</code> branch. A Software Engineering in Test job-family performance-indicator.
  target: TBD
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/vincywilson">Vincy Wilson</a>
  health:
    level: 0
    reasons:
      - Chart to track historical metric was broken. Chart has been recently fixed, but our visibility is limited.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/AverageAgeofQuarantinedEnd-to-EndTests
        height: 400px
        toolbar: hidden
        hide_tabs: true
  urls:
    - https://gitlab.com/gitlab-org/quality/team-tasks/issues/199
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/AverageAgeofQuarantinedEnd-to-EndTestsPI

- name: S1 Open Bug Age (OBA)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: S1 Open Bug Age (OBA) measures the total number of days that all S1 bugs are open within a month divided by the number of S1 bugs within that month.
  target: Below 60 days
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/vincywilson">Vincy Wilson</a>
  health:
    level: 3
    reasons:
      - Under target for the past 5 months
      - Moved to regular PI in FY24Q3
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/S1OBA
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: S2 Open Bug Age (OBA)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: S2 Open Bug Age (OBA) measures the total number of days that all S2 bugs are open within a month divided by the number of S2 bugs within that month.
  org: Quality Department
  target: Below 250 days
  is_key: false
  dri: <a href="https://gitlab.com/vincywilson">Vincy Wilson</a>
  health:
    level: 3
    reasons:
      - Under target for the past 11 months
      - Moved to regular PI in FY24Q3
  urls:
    - https://gitlab.com/gitlab-org/quality/quality-engineering/team-tasks/-/issues/1641
    - https://gitlab.com/gitlab-org/quality/quality-engineering/team-tasks/-/issues/1644
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/S2OBA
        height: 400px
        toolbar: hidden
        hide_tabs: true


############################
# Department Efficiency
############################

- name: Quality Handbook MR Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-merge-request-rate"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching <code>/source/handbook/engineering/quality/**</code> over time.
  target: Above 1 MR per person per month
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - Declining in last 3 months
      - To be combined into one handbook structure and measurement
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/HandbookMRRate/HandbookMRRate
        parameters:
          - field: Department/Division
            value: Quality

############################
## People Metrics
############################

- name: Quality Department Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the <a href="https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
      - Under target for 4 months, which is expected after being above target for 8 months
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/Promo-Q
        filters:
          - field: DPT Modified Department
            value: Internal Infrastructure

- name: Quality Department Discretionary Bonus Rate
  base_path: "/handbook/engineering/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - We have not been close to target for 10 months
      - Combining into one measurement in progress
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/R3DiscretionaryBonusRate
        filters:
          - field: DPT Modified Department
            value: Internal Infrastructure


############################
## Infrastructure Metrics
############################

- name: Infrastructure Handbook MR Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-mr-rate"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching `/source/handbook/engineering/**` or `/source/handbook/support/**` over time.
  target: .25
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
      - Adjusted the target to .55 to be consistent with larger org, reflect less activity from managers, and overall the trend that our initial suggested target is higher than many months of observed activity.
      - Combining into one handbook structure and measurement in progress
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/HandbookMRRate/HandbookMRRate
        parameters:
          - field: Department/Division
            value: Infrastructure

- name: Infrastructure Department Discretionary Bonus Rate
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
      - Combining into one department measurement in-progress
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/R3DiscretionaryBonusRate
        filters:
          - field: DPT Modified Department
            value: Core Infrastructure

- name: Mean Time Between Incidents (MTBI)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the mean elapsed time (in hours) from the start of one production incident, to the start of the next production incident. It serves primarily as an indicator of the amount of disruption being experienced by users and by on-call engineers. This metric includes only <a href="https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability">Severity 1 & 2 incidents</a> as these are most directly impactful to customers. This metric can be considered "MTBF of Incidents".
  target: more than 120 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/MeanTimeBetweenIncidentsPI
  health:
    level: 3
    reasons:
      - Target at 120 hours with the intent that we should not have such incidents more than approximately weekly (hopefully less).  Furter iterations will increase this target when we incorporate environment (production only).
      - Deployment failures (and the mean time between them) will be extracted into a separate metric to serve as a quality countermeasure for MTTP, unrelated to this metric which focuses on declared service incidents.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/MTBI
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: Mean Time To Resolution (MTTR)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: For all <a href="/handbook/engineering/monitoring/#gitlabcom-service-level-availability"> customer-impacting services</a>, measures the elapsed time (in hours) it takes us to resolve when an incident occurs. This serves as an indicator of our ability to execute said recoveries. This includes Severity 1 & Severity 2 incidents from <a href="https://gitlab.com/gitlab-com/gl-infra/production">production project</a>
  target: less than 24 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1717012?&label_name[]=incident
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8296
  - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/MeanTimetoResolutionPI
  health:
    level: 2
    reasons:
      - data depends on SREs adding incident::resolved label
      - as we continue to participate in dogfooding GitLab Incident Management we intend to improve this metric
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/MTTR
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: Mean Time To Mitigate (MTTM)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: For all <a href="/handbook/engineering/monitoring/#gitlabcom-service-level-availability"> customer-impacting services</a>, measures the elapsed time (in hours) it takes us to mitigate when an incident occurs. This serves as an indicator of our ability to mitigate production incidents. This includes Severity 1 & Severity 2 incidents from <a href="https://gitlab.com/gitlab-com/gl-infra/production">production project</a>
  target: less than 1 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/MeanTimetoMitigatePI
  health:
    level: 2
    reasons:
      - This metric is equivalent to the <i>Time to Restore</i> metric in the <a href="https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance">Four Keys Project from the DevOps Research and Assessment</a>
      - data depends on SREs adding incident::mitigate label
      - as we continue to participate in dogfooding GitLab Incident Management we intend to improve this metric
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/MTTM
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: GitLab.com Saturation Forecasting
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: It is critical that we continuously observe resource saturation normal growth as well as detect anomalies. This helps to ensure that we have the appropriate platform capacity in place. This metric uses the results of <a href="https://gitlab-com.gitlab.io/gl-infra/tamland/intro.html">Tamland</a> forecasting framework of non-horizontally scalable services, which end up as issues in <a href="https://gitlab.com/gitlab-com/gl-infra/capacity-planning/-/issues">Capacity Planning Project</a>. This metric counts the number of open capacity issues in that project.
  target: at or below 5 open issues
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
      - Next improvements are to document the existing process for creating capacity planning issues with a view to simplifying and automating the process. Documentation an improvement is a requirement for the <a href="https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/observation-management/-/issues/604">SOC 2 Availability Criteria</a> and is an <a href="https://app.ally.io/objectives/1890816?time_period_id=155985">OKR for Scalability for Q1<a/>.
      - Once we have <a href="https://gitlab.com/gitlab-data/analytics/-/issues/7713">Thanos data available in Snowflake</a> we will switch this PI to <a href="https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/63#note_597309238">show the percentage</a>
  urls:
    - "https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/63"
    - https://10az.online.tableau.com/#/site/gitlab/views/InfrastructureKPIs/SaturationForecastingPI
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/InfrastructureKPIs/SaturationForecasting
        height: 400px
        toolbar: hidden
        hide_tabs: true

- name: GitLab.com Hosting Cost / Revenue
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: We need to spend our investors' money wisely. As part of this we aim to follow industry standard targets for hosting cost as a % of overall revenue. In this case revenue is measured as MRR + one time monthly revenue from CI & Storage
  target: TBD
  public: false
  org: Infrastructure Department
  is_key: false
  health:
    level: -1
    reasons:
      - Confidential metric - See Key Review agenda
  urls:
    - "https://10az.online.tableau.com/#/site/gitlab/views/FinOps-CloudBilling/ExecutiveDashboard-1?:iid=1"

- name: Infrastructure Department Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the <a href="https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
      - Above target
      - Combining into one department measurement in-progress
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/Promo-Q
        filters:
          - field: DPT Modified Department
            value: Core Infrastructure
