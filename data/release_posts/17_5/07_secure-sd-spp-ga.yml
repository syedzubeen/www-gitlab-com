---
features:
  primary:
  - name: "Secret Push Protection is generally available"
    available_in: [ultimate]
    gitlab_com: true
    add_ons: []

    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/secret_detection/secret_push_protection'
    video: 'https://www.youtube-nocookie.com/embed/SFVuKx3hwNI'
    reporter: smeadzinger
    stage: application_security_testing
    categories:
      - 'Secret Detection'
    issue_url: # Multiple links are supported. Avoid linking to confidential issues.
      - 'https://gitlab.com/groups/gitlab-org/-/epics/13107'
    description: |
      We're excited to announce that Secret Push Protection is now generally available for all GitLab Ultimate customers.

      If a secret, like a key or an API token, is accidentally committed to a Git repository, anyone with access to the repository can impersonate the user of the secret for malicious purposes. A leaked secret costs time and money, and potentially damages a company's reputation. Secret push protection helps reduce the remediation time and reduce risk by protecting secrets from being pushed in the first place.

      Secret push protection has been improved since the beta release. When commits are pushed by using the Git CLI, now only the changes (diff) are scanned for secrets. We've also added experimental support for excluding paths, rules, or specific values to avoid false positives.

      To learn more, see [the blog](https://about.gitlab.com/blog/2024/06/24/prevent-secret-leaks-in-source-code-with-gitlab-secret-push-protection).
