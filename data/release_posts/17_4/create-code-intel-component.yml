features:
  secondary:
  - name: "CI/CD component for code intelligence"
    available_in: [core, premium, ultimate]  # Include all supported tiers
    documentation_link: 'https://docs.gitlab.com/ee/user/project/code_intelligence.html#with-the-cicd-component'
    gitlab_com: true
    reporter: phikai
    stage: create
    categories:
    - 'Code Review Workflow'
    issue_url: # Multiple links are supported. Avoid linking to confidential issues.
    - 'https://gitlab.com/gitlab-org/gitlab/-/issues/480401'
    description: |
      Code intelligence in GitLab provides code navigation features when browsing a repository. Getting started with code navigation is often complicated, as you must configure a CI/CD job. This job can require custom scripting to provide the correct output and artifacts.

      GitLab now supports an official [Code intelligence CI/CD component](https://gitlab.com/explore/catalog/components/code-intelligence) for easier setup. Add this component to your project by following the instructions for [using a component](https://docs.gitlab.com/ee/ci/components/index.html#use-a-component). This greatly simplifies adopting code intelligence in GitLab.

      Currently, the component supports these languages:

        - Go version 1.21 or later.
        - TypeScript or JavaScript.

      We'll continue to evaluate [available SCIP indexers](https://github.com/sourcegraph/scip?tab=readme-ov-file#tools-using-scip) as we look to broaden language support for the new component. If you're interested in adding support for a language, please open a merge request in the [code intelligence component](https://gitlab.com/components/code-intelligence) project.
