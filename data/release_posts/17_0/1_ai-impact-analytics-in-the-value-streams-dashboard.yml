---
features:
  primary:
  - name: "AI Impact analytics in the Value Streams Dashboard"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html#ai-impact-analytics'
    image_url: '/images/17_0/17.0_vsd_ai2.png'
    reporter: hsnir1
    stage: plan
    categories: 
      - Value Stream Management
      - Code Suggestions
    issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/12978'
    description: |
      AI Impact is a dashboard available in the Value Streams Dashboard that helps organizations understand the [impact of GitLab Duo on their productivity](https://about.gitlab.com/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/).
      This new month-over-month metric view compares the AI Usage trends with SDLC metrics like lead time, cycle time, DORA, and vulnerabilities. Software leaders can use the AI Impact dashboard to measure how much time is saved in their end-to-end workstream, while staying focused on business outcomes rather than developer activity.

      In this first release, the AI usage is measured as the monthly [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/) usage rate, and is calculated as the number of monthly unique Code Suggestions users divided by total monthly unique [contributors](https://docs.gitlab.com/ee/user/group/contribution_analytics/). 
      
      The AI Impact dashboard is available to users on the Ultimate tier for a limited time. Afterwards, a GitLab Duo Enterprise license will be required to use the dashboard.
