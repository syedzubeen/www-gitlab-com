---
features:
  primary:
  - name: "Auto-resolve vulnerabilities when not found in subsequent scans"
    available_in: [ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/vulnerability_management_policy.html'
    image_url: '/images/17_7/auto-resolve-when-not-found-in-subsequent-scan.png'
    reporter: dagron1
    stage: security_risk_management
    categories:
    - Vulnerability Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/5708'
    description: |
      GitLab's [security scanning tools](https://docs.gitlab.com/ee/user/application_security/#security-scanning-tools) help identify known vulnerabilities and potential weaknesses in your application code. Scanning feature branches surfaces new weaknesses or vulnerabilities so they can be remediated before merging. In the case of vulnerabilities already in your project's default branch, fixing these in a feature branch will mark the vulnerability as no longer detected when the next default branch scan runs. While it is informative to know which vulnerabilities are no longer detected, each must still be manually marked as Resolved to close them. This can be time consuming if there are many of these to resolve, even when using the new [Activity filter](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/#activity-filter) and [bulk-changing status](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/#change-status-of-vulnerabilities).

      We are introducing a new policy type _Vulnerability Management policy_ for users who want vulnerabilities automatically set to Resolved when no longer detected by automated scanning. Simply configure a new policy with the new Auto-resolve option and apply it to the appropriate project(s). You can even configure the policy to only Auto-resolve vulnerabilities of a certain severity or from specific security scanners. Once in place, the next time the project's default branch is scanned, any existing vulnerabilities that are no longer found will be marked as Resolved. The action updates the vulnerability record with an activity note, timestamp when the action occurred, and the pipeline the vulnerability was determined to be removed in.
