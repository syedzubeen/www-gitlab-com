---
features:
  primary:
  - name: "Pipeline execution policy type"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/pipeline_execution_policies.html'
    image_url: '/images/17_2/pipeline-execution-policy-rp.png'
    reporter: g.hickman
    stage: software_supply_chain_security
    categories:
    - Security Policy Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/13266'
    description: |
      The pipeline execution policy type is a new type of [security policy](https://docs.gitlab.com/ee/user/application_security/policies/) that allows users to support enforcement of generic CI jobs, scripts, and instructions.

      The pipeline execution policy type enables security and compliance teams to enforce customized [GitLab security scanning templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates/Jobs), [GitLab or partner-supported CI templates](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates), 3rd party security scanning templates, custom reporting rules through CI jobs, or custom scripts/rules through GitLab CI.

      The pipeline execution policy has two modes: inject and override. The _inject_ mode injects jobs into the project's CI/CD pipeline. The _override_ mode replaces the project's CI/CD pipeline configuration.

      As with all GitLab policies, enforcement can be managed centrally by designated security and compliance team members who create and manage the policies. [Learn how to get started by creating your first pipeline execution policy](https://docs.gitlab.com/ee/user/application_security/policies/pipeline_execution_policies.html)!
