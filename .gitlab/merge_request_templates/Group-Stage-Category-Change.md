<!--

Describe the change and rationale here.

-->

### Approvals

Merge requests with changes to stages and groups and significant
changes to categories need to be created, approved, and/or merged
by each of the below:

- [ ] Chief Product Officer `@david` (post MR link in chief-product-officer once all others have approved and tag `Gena Schwam` in slack) `@gschwam` for triage on behalf of David
- [ ] PLT Leader relevant to the affected Section(s) `@steve-evangelista OR @hbenson OR @mflouton OR @justinfarris`
- [ ] The Product Director relevant to the affected Section(s)
- [ ] The Engineering Director relevant to the affected Section(s)
- [ ] Director of Product Design `@vkarnes`

_**Note:** Chief Product Officer approval should be requested once all other approvals have been completed. To request approval, post the MR link in the #chief-product-officer channel tagging `@david` and cc'ing `@Gena Schwam`._

The following people need to be on the merge request so they stay informed:

- [ ] Chief Technology Officer `@sabrinafarmer`
- [ ] Development Leader relevant to the affected Section(s) `@timzallmann OR @bmarnane OR @meks`
- [ ] VP of Infrastructure & Quality Engineering `@meks`
- [ ] VP of UX `@ampesta`
- [ ] Director of Technical Writing `@susantacker`
- [ ] The Product Marketing Manager relevant to the stage group(s)

### After Approvals and Merge

- [ ] Update labels:
  - Create or rename labels under https://gitlab.com/groups/gitlab-org/-/labels and in any other group or project to reflect the new category/stage/group name.
  - If we are introducing a team split or consolidation, an automated label migration may be required. You can follow these steps:
    - [Create an issue](https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/new?issuable_template=category-label-change) for the `triage-ops` project to document the migration requirement, as well as any other necessary automation update.
    - Use GitLab Duo Workflow to self service this migration following [this handbook page](https://handbook.gitlab.com/handbook/engineering/infrastructure-platforms/developer-experience/development-analytics/create-triage-policy-with-gitlab-duo-workflow-guide). You can direct your qestions to `#g_development_analytics`.
- [ ] Open an MR in the `gitlab-org/gitlab` project to update any reference of the previous group label to the new one
- [ ] Mention the [product group Technical Writer](https://about.gitlab.com/handbook/product/ux/technical-writing/#designated-technical-writers) to update the [documentation metadata](https://docs.gitlab.com/ee/development/documentation/styleguide/#stage-and-group-metadata)
- [ ] Share MR in `#product`, `#development`, and relevant `#s_`, `#g_`, and `#f_` Slack channels
- [ ] Review direction pages, groups, projects, epics, issues, templates and documentation to ensure the name change is applied consistently
- [ ] (for group change only) Update the event and metric definitions belonging to the group by following [this guide](https://docs.gitlab.com/ee/development/internal_analytics/metrics/metrics_lifecycle.html#group-name-changes)

<!--
Changes that require executive approval include:
- Changes to a stage, group, or category name
- Removal or addition of a stage, group, or category

Changes that require approval only from the relevant Product Director include:
- Changing a category maturity date
- Changes to section or group member lists
- Changes to a category vision page

More information can be found in the Category Change section:
https://about.gitlab.com/handbook/product/categories/#changes

-->
