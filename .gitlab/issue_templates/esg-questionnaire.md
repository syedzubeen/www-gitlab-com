## ESG Questionnaire Template

**Please read these directions before submitting**

Please complete as much of the questionnaire as you can by reviewing publicly available information. Please see the [ESG section in Trust Center](https://trust.gitlab.com/?itemName=environment_social_governance&source=click) (all team members have a base-line entitlement), the [ESG Handbook page](https://handbook.gitlab.com/handbook/legal/esg/) and the most recent [ESG Report](https://handbook.gitlab.com/handbook/legal/esg/#esg-strategy), for answers to frequently asked questions.

**Customer and questionnaire details:**
Please complete the following customer information: 

1. Customer Name:
2. Link to Questionnaire:
3. GitLab product (GitLab Dedicated, GitLab.com, GitLab Self Managed, FedRamp, Other):
4. ARR amount (in USD) or Salesforce link:

**Please confirm that you have done the following prior to submitting this issue request:**

- [ ] I have reviewed the [ESG section in Trust Center](https://trust.gitlab.com/?itemName=environment_social_governance&source=click), the most recent [ESG Report](https://handbook.gitlab.com/handbook/legal/esg/#esg-strategy), and the [ESG handbook page](https://handbook.gitlab.com/handbook/legal/esg/) and filled in as much of the information as I can. 
- [ ] I have provided the requested customer details. 
- [ ] If the customer has specified a due date, please add that at the bottom of the issue under ‘Due Date.’ If a due date isn’t specified, we will aim to complete the questionnaire in no more than 10 business days. 

/assign @slcline, @kbuncle
/epic &2139
/label ~ESG::Ready for Review ~ESG Questionnaire
/confidential