---
layout: markdown_page
title: "Category Direction - Organization"
description: ""
canonical_path: "/direction/core_platform/tenant-scale/organization/"
---

- TOC
{:toc}

## Organization

| | |
| --- | --- |
| Stage | [Data Stores](https://about.gitlab.com/direction/core_platform/) |
| Maturity | [Minimal](https://about.gitlab.com/direction/#maturity) |
| Content Last Reviewed | `2024-08-20` |

## Introduction and how you can help

Thanks for visiting this category direction page on Organizations at GitLab.
The Organization category is part of the [Tenant Scale group](https://about.gitlab.com/handbook/product/categories/#tenant-scale-group) within the [Core Platform](https://about.gitlab.com/direction/core_platform/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute:
* Please comment in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=Category%3AOrganization&label_name%5B%5D=group%3A%3Atenant%20scale&first_page_size=100) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Organization&label_name[]=group::tenant+scale). Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy.
* Please share feedback directly via [email](https://gitlab.com/lohrc), or [schedule a video call](https://calendly.com/christinalohr/30min). If you're a GitLab user and have direct feedback about your needs for the organization, we'd love to hear from you.
* Please open an issue using the ~"Category:Organization" label, or reach out to us internally in the #g_tenant-scale Slack channel.

## Overview

The Organization category focuses on creating a better experience for organizations to manage their GitLab implementation.
This includes making administrative capabilities that previously only existed for self-managed users available to our SaaS users as well, for example management of users and groups and specific settings.
The result of this effort will be a more intuitive user experience for all our users towards more aligned behaviors throughout our product.

Organizations will offer the following functionality:

1. Centralized management: An Organization entity allows all groups, projects, and members to be managed under a single umbrella. This centralization simplifies administration, as it allows for easier configuration and maintenance of access controls, policies, and settings across the entire Organization.
   - An Organization can contain multiple top-level groups. For example, the following top-level groups would belong to the Organization GitLab:
     - https://gitlab.com/gitlab-org/
     - https://gitlab.com/gitlab-com/
   - Organizations remove the constraint of having a single hierarchy. An Organization is a container that could be filled with any collection of hierarchies that make sense.
   - Centralized control of user profiles. With an Organization-specific user profile, administrators can control the user’s role in a company, enforce user emails, or show a graphical indicator that a user is part of the Organization. An example could be adding a “GitLab employee” indicator on comments.
   - Unified UX and wider audience. Organizations allow us to better unify the experience on SaaS and self-managed deployments. Many instance-level features are admin only. We do not want to lock out users of GitLab.com in that way. We want to make administrative capabilities that previously only existed for self-managed users available to our GitLab.com users as well. The Organization Owner will have access to instance-equivalent settings with most of the configuration controlled at the Organization level. Instance-level workflows like dashboards can also be shifted to the Organization. This also means we would give users of GitLab.com more independence from GitLab.com admins in the long run. Today, there are actions that self-managed admins can perform that GitLab.com users have to request from GitLab.com admins, for instance banning malicious actors.
1. Isolation: In a cellular architecture, each Organization’s data, configurations, and resources are isolated. This is particularly important for customers in highly regulated industries, such as finance, healthcare, or government. Top-level Groups of the same Organization can interact with each other but not with Groups in other Organizations, providing clear boundaries for an Organization, similar to a self-managed instance. Isolation should have a positive impact on performance and availability as things like User dashboards can be scoped to Organizations.
1. Integration with Cells: Isolating Organizations makes it possible to allocate and distribute them across different Cells. The benefit of being on a cellular architecture is:
   - Increased reliability: A group of Organizations is fully isolated from other Organizations located on a different Cell. If an issue arises within one Organization, the impact is contained within the Cell the Organization is on, preventing a single point of failure from affecting the entire platform. This enhances the overall reliability of GitLab, reducing the risk of widespread outages and improving customer satisfaction. Having isolated Organizations is a pre-requisite to distribute customers amongst multiple Cells.

## Strategy and themes

The video below contains a high-level overview of what the Organization is trying to accomplish, some initial design considerations, and how Organizations build the foundation for Cells.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/ltFRc05cnbc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Today, GitLab's features exist at 3 levels:

| Level | Description | Example |
| ------ | ------ | ------ |
| Instance | Features for the entire instance. These are generally admin features (restricted to the Admin Area or via a config like `gitlab.rb`), but not always ([Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/)). | [LDAP](https://docs.gitlab.com/ee/administration/auth/ldap/#configuration-core-only), [admin-level push rules](https://docs.gitlab.com/ee/administration/admin_area.html#admin-area-sections) |
| Group | Features configured and used at the group level. These generally inherit behavior or objects down into subgroups (like epics, settings, or memberships). | [Epics](https://docs.gitlab.com/ee/user/group/epics/) |
| Project | Features used at the project level. | [Requirements Management](https://docs.gitlab.com/ee/user/project/requirements/) |

This leads to a few problems:
* 3 ways to build a feature lead to follow-on requests to build a feature at some other level and additional engineering effort.
* Restricts the audience, especially with instance-level features. Many of these features are admin-only, which is a tiny percentage of most users on an instance. If we make a feature instance level, we're locking ourselves into a few thousand self-managed users and locking out GitLab.com users.
* Poor UX. Inconsistencies between the features available at the project and group levels create navigation and usability issues. Moreover, there isn't a dedicated place for organization-level features.

While it would be ideal to have one way to build a new feature, most GitLab functionality should exist at the group or organization level rather than the instance level at a minimum.

We aim to extract features from the Admin Area into a new object called [Organization](https://docs.gitlab.com/ee/architecture/blueprints/organization/index.html) that acts as an umbrella to all the projects and groups it contains. 

## Goals

The [Organization](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/organization/) focuses on creating a better experience for organizations and enterprises to manage their GitLab experience. By introducing Organizations and [Cells](/direction/core_platform/tenant-scale/cell/) we can improve the reliability, performance and availability of our SaaS Platforms.

* Improved UX: Inconsistencies between the features available at the Project and Group levels create navigation and usability issues. Moreover, there isn’t a dedicated place for Organization-level features.
* Aggregation: Data from all Groups and Projects in an Organization can be aggregated.
* An Organization includes settings, data, and features from all Groups and Projects under the same owner (including personal namespaces).
* Cascading behavior: Organization cascades behavior to all the Projects and Groups that are owned by the same Organization. It can be decided at the Organization level whether a setting can be overridden or not on the levels beneath.
* Minimal burden on customers: The addition of Organizations should not change existing Group and Project paths to minimize the impact of URL changes.

### Near-term goals

Over the next few months, we are focussing on delivering the [Organization backend](https://gitlab.com/groups/gitlab-org/-/epics/14111). This serves as a foundation for [Cells](/direction/core_platform/tenant-scale/cell/) and for other teams to add functionality that is needed at the organization-level. The initial Organization release will contain the following functionality:

* Instance setting to allow the creation of multiple Organizations. This will be enabled by default on GitLab.com, and disabled for self-managed GitLab.
* Organization Owner. The creation of an Organization appoints that user as the Organization Owner. Once established, the Organization Owner can appoint other Organization Owners.
* Organization users. A user is managed by one Organization, but can be part of multiple Organizations. Users are able to navigate between the different Organizations they are part of.
* Organization settings page. Containing the Organization name, ID, description, and avatar. Settings are editable by the Organization Owner.
* Setup flow. Users are able to migrate existing top-level groups to newly created Organizations. New users are able to create an Organization from scratch and start building top-level groups from there.
* Visibility. Initially, Organizations will be restricted to be private, containing only private top-level groups. Visibility is editable by the Organization Owner. Public visibility is planned for a later iteration.
* Groups. This includes the ability to create, edit, and delete groups, as well as a Groups overview that can be accessed by the Organization Owner.
* Projects. This includes the ability to create, edit, and delete projects, as well as a Projects overview that can be accessed by the Organization Owner.

### Mid-term goals

Once established, we want to focus on improving workflows and functionality for users of the Organiztion by addressing the following gaps:

* Internal visibility will be made available on Organizations that are part of GitLab.com. Currently, `internal` visibility is only available on self-managed GitLab, and the only way for customers of GitLab.com to shield their content is to set the visibility of all of their groups and projects to `private`. This setting is often too restrictive for users to easily find their way around an Organization. By making `internal` visibility available, administrators can enable their users to safely explore projects without having to supervise their membership.

## 1 year plan

Within the next year, we are planning to release the [Organization](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/organization/).

### What we are currently working on

Our immediate focus is to release [Organizations with limited funtionality for Cells 1.0](https://handbook.gitlab.com/handbook/engineering/architecture/design-documents/organization/#organizations-on-cells-10-fy24q2-fy25q4) containing:

- [Organization backend](https://gitlab.com/groups/gitlab-org/-/epics/14111)
- [Organization navigation](https://gitlab.com/groups/gitlab-org/-/epics/11190)
- [Organization isolation](https://gitlab.com/groups/gitlab-org/-/epics/11670)

### What is planned next

- [Organization users](https://gitlab.com/groups/gitlab-org/-/epics/12293)

### What we recently completed

- [Organization front page](https://gitlab.com/groups/gitlab-org/-/epics/11187)
- [Organization project overview](https://gitlab.com/groups/gitlab-org/-/epics/11189)
- [Organization group overview](https://gitlab.com/groups/gitlab-org/-/epics/11188)
- [Organization creation](https://gitlab.com/groups/gitlab-org/-/epics/11191)

### What is not planned right now

- We are currently not considering making the Organization part of the namespace framework.
